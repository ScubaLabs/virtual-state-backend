angular
    .module('vEstate')
    .controller('displayUser', ['$scope', '$http',function($scope,$http){
    	$scope.user="";
    	$http.get('/getuser').then(
    		function(response){
    			$scope.user='"'+response.data.user.firstName+' '+response.data.user.lastName+'"';
    		},function(response){
    			$scope.user='';
    		}
		);
    }]);