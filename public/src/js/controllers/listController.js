// An Angular controller for Retrieving all the records from the Database
angular
    .module('vEstate')
    .controller('listController', ['$scope', '$http', function($scope, $http) {
    	$scope.heading= '';
	    $scope.status= '';
	    $scope.displaydata='';
	    $scope.errordisplay='';
	    // A function to make a request for Retrieving and displaying all the records from the database
	    $scope.displayData = function(){
		    $http.get('/data').
			    then(function(response){
			        $scope.heading="List of Markers";
			        $scope.status=response.status;
			        $scope.displaydata=angular.fromJson(response.data);
			        //console.log("success");
			        //console.log($scope.displaydata);
			    },function(response){
			        $scope.heading="";
			        $scope.status=response.status;
			        $scope.displaydata=response.data || 'Request failed';
			        //console.log("error");
			    })
	    };
	    $scope.displayData(); // A call to the function defined above
	    // A function to make a request for Deleting a specific record from the database
	    $scope.deleteData = function(id,targetId){
		    $http.delete('/data/'+id+'/'+targetId).
		    then(function(response){
		        //console.log("success");
		        //console.log(response);
		        $scope.displayData();
		        $scope.errordisplay="Delete successful";

		    },function(response){
		        //console.log(response);
		        $scope.errordisplay="Wait for sometime as image uploading is still in progress over Vuforia server, once the upload is finished you will be able to delete the entry";
		        $scope.displayData();
	        });
    	}

    	// A function which redirects user to another page which has a form to get the new values required for updating a specific record
    	$scope.editData = function(id){
    		window.location.href='/#/edit/'+id;
    	}
    }]);