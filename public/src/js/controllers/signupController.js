angular
    .module('vEstate')
    .controller('signupController', ['$scope', '$location', 'AuthService',
  function ($scope, $location, AuthService) {
    	$scope.firstname='';
    	$scope.lastname='';
    	$scope.email='';
    	$scope.username='';
    	$scope.password='';
    	$scope.rpassword='';
    	$scope.info='';
    	$scope.doSignup = function(){
    		if($scope.firstname && $scope.lastname && $scope.email && $scope.username && $scope.password && $scope.rpassword){
		    	// initial values
				$scope.error = false;
				$scope.disabled = true;
				if($scope.rpassword==$scope.password){
					// call register from service
					AuthService.register($scope.firstname, $scope.lastname, $scope.email, $scope.username, $scope.password)
					// handle success
					.then(function () {
						$scope.errorMessage = "Signed Up Successfully!";
						$scope.disabled = false;
						$scope.firstname='';
				    	$scope.lastname='';
				    	$scope.email='';
				    	$scope.username='';
				    	$scope.password='';
				    	$scope.rpassword='';
				    	$scope.info='';
				    	window.location.href='/';
					},function () {
						$scope.error = true;
						$scope.disabled = false;
						$scope.errorMessage = "User Name or E-mail exists!";
						$scope.email='';
				    	$scope.username='';
					});
				}
			}
    	}
    }]);



