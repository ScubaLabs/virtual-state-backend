// An Angular controller for editing and deleting record(s) from the Database
angular
    .module('vEstate')
    .controller('editController', ['$scope', '$http', '$stateParams', function($scope, $http, $stateParams) {
    	$scope.id=$stateParams.id;
    	$scope.displayForNow = '';
		$scope.status= '';
		$scope.data= '';
		$scope.clientname='';
		$scope.projectname='';
		$scope.markername='';
		$scope.website='';
		$scope.modeltype='';
		$scope.updateddate=new Date().toDateString();
		//console.log($scope.updateddate);
		// A function to make a request for retrieval of a specific record in the Database 
		$scope.get = function(){
		    //console.log("Requested Data List");
		    $http.get('/data/'+$scope.id).
		    then(function(response){
		        $scope.status=response.status;
		        $scope.clientname=response.data.clientName;
		        $scope.projectname=response.data.projectName;
		        $scope.markername=response.data.markerName;
		        $scope.website=response.data.website;
		        $scope.modeltype=response.data.modelType;
		        //console.log("success");
		    },function(response){
		        $scope.status=response.status;
		        //console.log("error");
		    })
	    };
	    $scope.get();// Call of the above defined function
	    // A function to make a request for editing and updating a specific record in the database
		$scope.updateDetails = function(){
			//console.log("Entered");
			$scope.displayForNow='Entered';
			$http.put('/data/'+$scope.id ,{clientName: $scope.clientname, projectName: $scope.projectname, markerName: $scope.markername, website: $scope.website, modelType: $scope.modeltype, updatedDate: $scope.updateddate, isActive: true})
			.then(function(response){
				$scope.status=response.status;
				$scope.data=response.data;
				$scope.displayForNow="Data Successfully Updated";
				//console.log("Success");
			},function(response){
				$scope.status=response.status;
				$scope.data=response.data || 'Request failed';
				$scope.displayForNow="Error while Updating";
				//console.log("Error");
			})
		};
    }]);