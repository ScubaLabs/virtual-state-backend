angular
    .module('vEstate')
    .controller('changePassword', ['$scope', '$http',function($scope,$http){
    	$scope.user="";
        $scope.userName='';
        $scope.password='';
        $scope.cpassword='';
        $scope.message='';
    	$http.get('/getuser').then(
    		function(response){
    			$scope.user=response.data.user.firstName+' '+response.data.user.lastName;
                $scope.userName=response.data.user.userName;
    		},function(response){
    			$scope.user='';
    		}
		);
        $scope.changePassword = function(){
            if(($scope.password===$scope.cpassword) && $scope.password){
                $http.post('/change',{userName: $scope.userName, password:$scope.password}).then(
                    function(response){
                        $scope.message=response.data.message+' !!';
                        $scope.password='';
                        $scope.cpassword='';
                    },function(response){
                        $scope.message='Error while changing password. Try again!'
                        $scope.password='';
                        $scope.cpassword='';
                    });
            }else if(!$scope.password || !$scope.cpassword){
                $scope.message='You can not leave any of the field empty!';
            }else{
                $scope.message='Passwords do not match!';
            }
        }
    }]);