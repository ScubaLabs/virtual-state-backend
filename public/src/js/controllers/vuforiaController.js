// Controller to fetch parameters from the URL and fill the form automatically in the page for uploading Target marker image on Vuforia
angular
    .module('vEstate')
    .controller('vuforiaController', ['$scope', '$http', '$stateParams', function($scope, $http, $stateParams) {
    	$scope.rowid = $stateParams.rowid;
      $scope.markername = $stateParams.markername;
    	$scope.width = 32;
    	$scope.file = '';
		if($stateParams.error){
            if($stateParams.error=="ImageTooLarge")
                $scope.errorMessage = 'Image Too Large, choose another file.'
            else
                $scope.errorMessage = $stateParams.error;
        }
        else
            $scope.errorMessage = '';
		
    	$http.get('/data/'+$scope.rowid).then(
    		function(response){
    				//console.log(response);
    				$scope.clientname=response.data.clientName;
    				$scope.projectname=response.data.projectName;
    				$scope.modeltype=response.data.modelType;
    			},
			function(response){
					//console.log(response);
				});
    }]);