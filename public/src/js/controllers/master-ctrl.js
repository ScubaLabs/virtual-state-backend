/**
 * Master Controller
 */

angular
    .module('vEstate')
    .controller('MasterCtrl', ['$scope', '$http', '$cookieStore','$location','AuthService', function($scope, $http, $cookieStore, $location, AuthService) {
    /**
     * Sidebar Toggle & Cookie Control
     */
    var mobileView = 992;
    $scope.status=false;
    var checkStatus = function(){
        $http.get('/status').then(
                function(response){
                    if(response.data.status==true){
                        $scope.status=true;
                    }
                    else{
                        $scope.status=false;
                    }
                },function(response){
                    $scope.status=false;
                }
            );
    }
    
    
    $scope.logout = function () {
        checkStatus();
      // call logout from service
        setTimeout(function(){if($scope.status==true){
            AuthService.logout()
            .then(function () {
                window.location.href='/checkpoint.html';
            });
        }
        else{
            window.alert("You must login first");
        }},1000);

    };

    $scope.getWidth = function() {
        return window.innerWidth;
    };

    $scope.$watch($scope.getWidth, function(newValue, oldValue) {
        if (newValue >= mobileView) {
            if (angular.isDefined($cookieStore.get('toggle'))) {
                $scope.toggle = ! $cookieStore.get('toggle') ? false : true;
            } else {
                $scope.toggle = true;
            }
        } else {
            $scope.toggle = false;
        }

    });

    $scope.toggleSidebar = function() {
        $scope.toggle = !$scope.toggle;
        $cookieStore.put('toggle', $scope.toggle);
    };

    window.onresize = function() {
        $scope.$apply();
    };
}]);







   