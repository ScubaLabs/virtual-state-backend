// An Angular controller for inserting record(s) in the Database
angular
    .module('vEstate')
    .controller('formController', ['$scope', '$http', function($scope, $http) {
    	$scope.displayForNow = '';
		$scope.status= '';
		$scope.data= '';
		$scope.clientname='';
		$scope.projectname='';
		$scope.markername='';
		$scope.website='';
		$scope.modeltype='3D Model';
		$scope.createddate=new Date().toDateString();
		$scope.updateddate=new Date().toDateString();
		// A function to make a request for inserting a new record in the database
		$scope.enterDetails = function(){
			// A validation in the form to check whether all the fields in the form is filled out or not
			if($scope.clientname && $scope.projectname && $scope.markername){
				//console.log("Entered");
				$http.post('/data',{clientName: $scope.clientname, projectName: $scope.projectname, markerName: $scope.markername, website: $scope.website, modelType: $scope.modeltype, createdDate: $scope.createddate, updatedDate: $scope.updateddate, isActive: true})
				.then(function(response){
					$scope.status=response.status;
					$scope.data=response.data._id;
					$scope.displayForNow="Data Successfully entered";

					console.log($scope.data);
					window.location.href='/#/vuforia/'+$scope.markername+'/'+$scope.data;

					//console.log(response);
				//	window.location.href='/#/vuforia/'+$scope.markername+'/'+$scope.data;

				},function(response){
					$scope.status=response.status;
					$scope.data=response.data || 'Request failed';
					$scope.displayForNow="Error while Entering. Please try with different Marker Name.";
					//console.log(response);
				});
			}else
				{
					$scope.displayForNow="You must fill out all the fields to go further.";
				}
		};
		
    }]);