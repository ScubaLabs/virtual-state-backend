// Controller to fetch parameters from the URL and fill the form automatically in the page for uploading Model(s) over the server
angular
    .module('vEstate')
    .controller('modeluploadController', ['$scope', '$http', '$stateParams', function($scope, $http, $stateParams) {
    	$scope.targetid = $stateParams.targetid; 
    	$scope.rowid = $stateParams.rowid;
        $scope.modeltype = '';
        var i=0;
        var materialIncluded=false;
        $scope.getModelType = function(){
            $http.get('/data/'+$scope.rowid).
                then(function(response){
                    $scope.modeltype=response.data.modelType;
                },function(response){
                    $scope.getModelType();
                })
        };
        $scope.getMaterial = function(k){
            var string = document.getElementById("model"+k).value;
            var n =string.length;
            if((string[n-4]+string[n-3]+string[n-2]+string[n-1])==".obj"&&materialIncluded==false)
            {
                var materialElement = '<br class="linebreak"><td class="matField"><b class="test">Material File:</b></td><td><input class="mat" type="file" name="material" required/></td>';
                $('#modelNew').append(materialElement);
                //console.log(materialElement);
                materialIncluded=true;
            }
            else{
                document.getElementsByClassName("test")[0].remove();
                $('.matField').remove();
                $('.mat').remove();
                $('.linebreak').remove();
                materialIncluded=false;
            }
        }
        $scope.getModelType();
        //Function to give user a option for uploading more than one model by adding one more file input field in the form 
		$scope.moreModel = function()
    	{	
            i++;
            var newModel = angular.element( document.querySelector( '#modelNew' ) );
    		var el = '<br><td><input type="file" name="model" id="model'+i+'" ng-model="model['+i+']" onchange="angular.element(this).scope().getMaterial('+i+')" required/></td>';
    		//console.log(el);
    		newModel.append(el);
            materialIncluded=false;
        }
    }]);