angular.module('vEstate').factory('AuthService',['$q', '$timeout', '$http',
    function ($q, $timeout, $http) {

        // create user variable
        var user = null;

        function isLoggedIn() {
      if(user) {
        return true;
      } else {
        return false;
      }
    }

    function getUserStatus() {
       return $http.get('/status')
      // handle success
      .success(function (data) {
        if(data.status){
          user = true;
        } else {
          user = false;
        }
      })
      // handle error
      .error(function (data) {
        user = false;
      });
    }

    function login(username, password) {

      // create a new instance of deferred
      var deferred = $q.defer();

      // send a post request to the server
      $http.post('/login',
        {username: username, password: password})
        // handle success
        .then(function (response) {
          if(response.status === 200 && response.data.status=="Login successful!"){
            user = true;
            deferred.resolve();
            userLoggedIn=response.data.user;
            window.location.href='/#/home';
          } else {
            user = false;
            deferred.reject();
          }
        },function (response) {
          //console.log(response.data);
          user = false;
          deferred.reject();
        });

      // return promise object
      return deferred.promise;

    }

    function logout() {
      if(user==true){
        // create a new instance of deferred
        var deferred = $q.defer();

        // send a get request to the server
        $http.get('/logout')
          // handle success
          .success(function (data) {
            user = false;
            deferred.resolve();
          })
          // handle error
          .error(function (data) {
            user = false;
            deferred.reject();
          });

        // return promise object
        return deferred.promise;
      }
    }

    function register(firstname, lastname, email, username, password) {

      // create a new instance of deferred
      var deferred = $q.defer();

      // send a post request to the server
      $http.post('/signup',
        {firstName: firstname, lastName: lastname, email: email, userName: username, password: password})
        // handle success
        .then(function (response) {
          if(response.status === 200 && response.data.status){
            //console.log("success");
            deferred.resolve();
          } else {
            //console.log("failure");
            deferred.reject();
          }
        },function (data) {
          //console.log("failure");
          deferred.reject();
        });

      // return promise object
      return deferred.promise;

    }

    // return available functions for use in the controllers
    return ({
      isLoggedIn: isLoggedIn,
      getUserStatus: getUserStatus,
      login: login,
      logout: logout,
      register: register
    });
}]);

