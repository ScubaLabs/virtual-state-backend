'use strict';

/**
 * Route configuration for the vEstate module.
 */
angular.module('vEstate').config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {

        // For unmatched routes
        $urlRouterProvider.otherwise('/home');

        // Application routes
        $stateProvider
        // Default Page route 
            .state('index', {
                url: '/',
                templateUrl: 'templates/home.html',
                access: {restricted: true}
            })
            .state('home', {
                url: '/home',
                templateUrl: 'templates/home.html',
                access: {restricted: true}
            })
        // Route of page to enter primary details of the marker and model
            .state('enter', {
                url: '/enter',
                templateUrl: 'templates/enter.html',
                access: {restricted: true}
            })
            .state('logout', {
                url: '/logoutscreen',
                templateUrl: 'templates/home.html',
                access: {restricted: true}
            })
            .state('changepass', {
                url: '/change',
                templateUrl: 'templates/change.html',
                access: {restricted: true}
            })
        // Route of the page to upload marker over the Vuforia REST services
            .state('vuforia', {
                url: '/vuforia/:markername/:rowid',
                templateUrl: 'templates/vuforia.html',
                access: {restricted: true}
            })
            .state('vuforiaerror', {
                url: '/vuforia/:markername/:rowid/:error',
                templateUrl: 'templates/vuforia.html',
                access: {restricted: true}
            })
        // Route of the page to edit the basic details of the Model
            .state('edit', {
                url: '/edit/:id',
                templateUrl: 'templates/edit.html',
                access: {restricted: true}
            })
        // Route of the page to upload the marker over the server
            .state('modelupload', {
                url: '/modelupload/:targetid/:rowid',
                templateUrl: 'templates/modelupload.html',
                access: {restricted: true}
            })
        // Route of the page to enlist all the entries in the database
            .state('list', {
                url: '/data',
                templateUrl: 'templates/list.html',
                access: {restricted: true}
            }).state('entervr', {
                url: '/entervr',
                templateUrl: 'templates/entervr.html',
                access: {restricted: true}
            });
    }
]).run(function ($rootScope, $location, $state, AuthService) {
  $rootScope.$on('$stateChangeStart',
    function (event, next, current) {
      AuthService.getUserStatus()
      .then(function(){
        if(next.access.restricted && !AuthService.isLoggedIn()){
            window.location.href='/checkpoint.html';
            $state.reload(); 
        }
      });
  });
});