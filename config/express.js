var config = require('./config'),
    passport = require('passport'),
 	bodyParser = require('body-parser'),
	express = require('express');
	flash = require('connect-flash'),
	session = require('express-session')
    fileUpload = require('express-fileupload'),
    async = require('async'),
    crypto = require('crypto'),
    compression = require('compression');




module.exports = function() 
{
    var app = express();

    app.use(bodyParser.urlencoded({
        extended: true
    }));

    app.use(bodyParser.json());
    app.use(express.static('public/src'));
    app.use(express.static('assets/models'));
    app.use(compression());

	app.use(session({
    	saveUninitialized: true,
    	resave: true,
    	secret: 'loginCookieForVirtualEstate'
	}));
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(passport.initialize());
    app.use(passport.session());
    require('../config/passport.js')(passport);

	app.use(flash());
    
    app.use(fileUpload());
    app.set('views', './app/views');
	app.set('view engine', 'ejs');

    require('../app/routes/marker.server.routes.js')(app);
    require('../app/routes/user.server.routes.js')(app);
    require('../app/routes/modelsvr.server.routes.js')(app);
    require('../app/routes/modelsvr.server.routes.js')(app);
    app.use(express.static('./public'));

    return app;
};  