var config = require('./config'),
    mongoose = require('mongoose');
require('../app/models/marker.model');
require('../app/models/user.model');
require('../app/models/modelsvr.model');
require('../app/models/virtualmodel.model');

module.exports = function() {
    var db = mongoose.connect(config.db);
    return db;
};