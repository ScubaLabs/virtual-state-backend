var passport = require('passport'),
    mongoose = require('mongoose');

module.exports = function(passport) {
    var User = mongoose.model('UserDB');

    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {
        User.findOne(
            {_id: id},
            '-password',
            function(err, user) {
                done(err, user);
            }
        );
    });

    require('../config/strategies/local.js')();
};