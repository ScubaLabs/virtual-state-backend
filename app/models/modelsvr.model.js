var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/* An schema for the MongoDB collection which will be used in the backend for 
the purpose of storing the data entered by the users*/ 
var ModelsVRSchema = new Schema({
    projectName: 
        {
            type: String,
            required: true,
        },
    projectPhoto: 
        {
            type: String,
            required: true,
        },
    description: 
        {
            type: String,
            required: true,
        },
    interiorDesign: String,
    script: String,
    show: String,
    createdDate: {
        type: Date,
        default: Date.now
    },
    updatedDate: {
        type: Date,
        default: Date.now
    },
    isActive: Boolean,
    userName: String
});

// A model object is created for the Database based on the above schema
mongoose.model('ModelsVRDB', ModelsVRSchema);