var mongoose = require('mongoose'),
 	crypto = require('crypto'),
    Schema = mongoose.Schema;

var ScoreSchema = new Schema({
    model_id: String,
    modelname: String,
    initial_position: Object,
    initial_rotation: Object,
    paths:   { type : Array , "default" : [] },
    created_at: {type: Date, required: true, default: Date},
    updated_at: {type: Date, required: true, default: Date},
    isactive:   {type: Boolean, default: true}
},
{
    versionKey: false //Setting document version to false
}
);

mongoose.model('VirtualModel', ScoreSchema);