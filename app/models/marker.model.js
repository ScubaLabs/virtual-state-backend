var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/* An schema for the MongoDB collection which will be used in the backend for 
the purpose of storing the data entered by the users*/ 
var ModelSchema = new Schema({
    clientName: 
        {
            type: String,
            required: true,
        },
    projectName: 
        {
            type: String,
            required: true,
        },
    markerName: 
        {
            type: String,
            required: true,
            unique: true, 
            lowercase: true, 
            trim: true
        },
    nameModel: [],
    modelId: String,
    website: String,
    modelType: String,
    createdDate: {
        type: Date,
        default: Date.now
    },
    updatedDate: {
        type: Date,
        default: Date.now
    },
    isActive: Boolean,
    userName: String
});

// A model object is created for the Database based on the above schema
mongoose.model('ModelDB', ModelSchema);



