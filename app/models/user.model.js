var mongoose = require('mongoose'),
    crypto = require('crypto'),
    Schema = mongoose.Schema;

/* An schema for the MongoDB collection which will be used in the backend for 
the purpose of storing the data entered by the users*/ 
var UserSchema = new Schema({
    firstName:
        {
            type: String,
            required: true,
        },
    lastName: 
        {
            type: String,
            required: true,
        },
    email: 
        {
            type: String,
            required: true,
            unique: true,
        },
    userName: 
        {
            type: String,
            required: true,
            unique: true,
        },
    password:
        {
            type: String,
            required: true,
        },

    resetPasswordToken: String,
    resetPasswordExpires: Date
});

UserSchema.methods.authenticate = function(password) {
    
    if(this.password===password)
    return true;
};

// A model object is created for the Database based on the above schema
mongoose.model('UserDB', UserSchema);
