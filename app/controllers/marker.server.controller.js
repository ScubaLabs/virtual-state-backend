
var ModelDB = require('mongoose').model('ModelDB');
// function to create a record in the MongoDB collection
exports.create = function(req,res,next){
	var Model = new ModelDB(req.body);
	Model.save(function(err,markerrow){
		if(err){
			//return(next(err));
            res.status(500).send(err);
		} else{
			res.send(markerrow);
		}
	});
};

// function to get all the records in the MongoDB collection
exports.list = function(req, res, next) 
{
    if(req.user)
    {
        ModelDB.find({userName: req.user.userName}, function(err, markers){
            if (err) {
                //return next(err);
                res.status(500).send(err);
            }
            else {
                res.json(markers);
            }
        });
    }
    else
        res.redirect('/#/home');
};

// function to delete a record in the MongoDB collection with matching ID
exports.delete = function(req, res, next) 
{   
    ModelDB.findByIdAndRemove(req.params.markerId, function(err, marker){
        if (err) {
            //return next(err);
            res.status(500).send(err);
        }
        else {
            res.json(marker);
        }
    })
};

// function to retrieve a particular record in the MongoDB collection with a matching ID
exports.read = function(req, res) {
    ModelDB.findById(req.params.markerId, function(err, marker){
        if (err) {
            //return next(err);
            res.status(500).send(err);
        }
        else {
            res.json(marker);
        }
    });

};

/* function to retrieve a particular record in the MongoDB collection with a matching Marker 
Target ID mapped in the MongoDB collection's record when the api is hitted in the app*/
exports.readforapp = function(req, res) {
    ModelDB.find({modelId: req.params.id}, function(err, marker){
        if (err) {
            //return next(err);
            res.status(500).send(err);
        }
        else {
            var resultObj = {result : marker};
            res.json(resultObj);
        }
    });

};

// function to update an existing record in the MongoDB collection with a matching ID 
exports.update = function(req, res, next) 
{
    ModelDB.findByIdAndUpdate(req.params.markerId, req.body, function(err, marker) 
    {
        if (err) {
            //return next(err);
            res.status(500).send(err);
        }
        else {
            //console.log("updated",marker); 
        }
    });
};

// function to Insert Model names in the Record on Uploading of Models.
exports.modelUpdate = function(rowid, object) 
{
    ModelDB.findByIdAndUpdate(rowid, object, function(err, marker) 
    {
        if (err) {
            //console.log(err);
        }
        else {
            //console.log("row updated",marker); 
        }
    });
};
