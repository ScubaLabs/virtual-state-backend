var UserDB = require('mongoose').model('UserDB');
// function to create a record in the MongoDB collection
exports.create = function(req,res,next){
	var User = new UserDB(req.body);
	User.save(function(err,userdata){
		if(err){
			return(next(err));
		} else{
			req.logIn(userdata,function(err){
				if(err) res.redirect('/'); 
				res.status(200).json({status:true, user:userdata.userName});
			});
		}
	});
};

exports.passwordChange = function(req, res, next) {
    UserDB.findOne({userName: req.body.userName}, function(err, userdata){
        if (err) {
            return next(err);
        }
        else {
            userdata.password=req.body.password;
            userdata.save(function(err) {
        		if(err)
        			next(err);
        	});
            res.json({message: 'Password changed'});
        }
    });

};