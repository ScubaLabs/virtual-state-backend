var ModelsVRDB = require('mongoose').model('ModelsVRDB');
var VirtualModel = require('mongoose').model('VirtualModel');

exports.create = function(req,res){
	var Vr = new ModelsVRDB(req);
	Vr.save(function(err,Vrrow){
		if(err){
			//return(next(err));
            res.status(500).send(err);
		} else{
			console.log(Vrrow)
			res.redirect('/');
		}
	});
};


exports.createNavigation = function(req, res, next) 
{	
	console.log(req.body);
	var VrModel = new VirtualModel(req.body);
	VrModel.save(function(err,result){
		if(err){
			 res.json({"Status":"Failed to upload the model", "Error":err});
            
		} else{
			 res.json(result);
		}
	});
    
};

exports.getNavigationData = function(req, res, next) 
{   
   console.log("getNavigationData called");
   VirtualModel.findOne({
            _id: req.params.Id
        }, function(err, results){
        if (err) {
            res.json({"Status":"Failed to get the models", "Error":err});
        }
        else {
            res.json(results);
        }
    });
};

exports.deleteNavigationData = function(req, res, next) 
{   
   console.log("deleteNavigationData called");
   VirtualModel.findByIdAndRemove(req.params.Id, function(err, result){
        if (err) {
             res.json({"Status":"Failed to delete the model", "Error":err});
        }
        else {
            res.json({"Status":"Deleted Successfully"});
        }
    })
};