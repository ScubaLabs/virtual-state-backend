var marker = require('../../app/controllers/marker.server.controller');
var formidable = require('formidable');
var fs = require('fs');
var path = require('path'),
    vuforia = require('vuforiajs');
require('isomorphic-fetch'); // or another library of choice.
var Dropbox = require('dropbox').Dropbox;
var waterfall = require('async-waterfall');
// An object to create connection with Vuforia server for uploading the Target image marker
var client = vuforia.client({

    // server_access_key
    // server_access_key
    //'accessKey': 'e33f4fcf336f24932fb51a5fb931a47aae70d8bb', //- older access key - account suspended
	'accessKey': '88c75e70b8f5430989acf61017e613964dad067b',

    // server_secret_key
    //'secretKey': '40b94c56e035fa1f787e75e5ef98569c5258f39d' //- older access key - account suspended
	'secretKey': 'f034b179f3d81dfd175a23bf4c19635753f6d7b7'

   // 'accessKey': 'a1bfe0ebbdced28370ea06d210055ba98cb623bd',

    // server_secret_key
   // 'secretKey': 'f8aa3c15e7fd7febd1bd5652591e8e92a130f6cf'

});

var util = vuforia.util();

// funtion to provide user what is required through various routes 
module.exports = function(app){
	// Route for inserting a record in the database
	app.route('/data').post(marker.create);
	// Route for retrieving a record in the database
	app.route('/data').get(marker.list);
	/* Route along with implemented funtion for uploading an Target image marker on Vuforia through 
	its REST services*/
	app.route('/upload').post(function(req,res){
    	if (!req.files.file)
				res.redirect('/#/vuforia/'+req.body.markername+'/'+req.body.rowid+'/Select A Image First');// redirecting the user to the same page
		else if (req.files.file.mimetype=='image/jpeg' || req.files.file.mimetype=='image/png' ){
			var file = req.files.file;
			var re = /(?:\.([^.]+))?$/;
			var ext = re.exec(req.files.file.name)[1];
			var path = './assets/markers/'+req.body.markername+"."+ext;
			//moving a copy of Target marker image on our server
			file.mv(path, function(err) {
				if (err)
				  res.redirect('/#/vuforia/'+req.body.markername+'/'+req.body.rowid+'/'+err);// redirecting the user to the same page
				// Object to be sent on Vuforia server to upload the Image
				var target = {
				    // name of the target, unique within a database
				    'name': req.body.markername,
				    // width of the target in scene unit
				    'width': parseInt(req.body.width),
				    // the base64 encoded binary recognition image data
				    'image': util.encodeFileBase64(path),
				    // indicates whether or not the target is active for query
				    'active_flag': true,
				    // the base64 encoded application metadata associated with the target
				    'application_metadata': util.encodeBase64('<?xml version="1.0" encoding="UTF-8"?><root><clientName>'+req.body.clientname+'</clientName><modelType>'+req.body.modeltype+'</modelType><projectName>'+req.body.projectname+'</projectName></root>')
				};
				// A request to the Vuforia server to upload the Target marker Image
				client.addTarget(target, function (error, result) {
				    if (error) { // e.g. [Error: AuthenticationFailure]

				    	console.error(result);
				        res.redirect('/#/vuforia/'+req.body.markername+'/'+req.body.rowid+'/'+result.result_code);// redirecting the user to the same page

				    	//console.error(result);
				     //   res.redirect('/#/vuforia/'+req.body.markername+'/'+req.body.rowid);// redirecting the user to the same page

				        /* result would look like
				         {
				            result_code: 'AuthenticationFailure',
				            transaction_id: '58b51ddc7a2c4ac58d405027acf5f99a'
				         }
				         */
				    } else {
			    		// res.json("'result': 'success'");
				        //console.log(result+": Target image successfully uploaded.... Redirecting to Homepage....");
			        	result.target_id
			     		res.redirect('/#/modelupload/'+result.target_id+'/'+req.body.rowid);// redirecting the user to the next page
			     		
				        /* result will look like
				         {
				            target_id: '93fd6681f1r74b76bg80tf736a11b6a9',
				            result_code: 'TargetCreated',
				            transaction_id: 'xf157g63179641c4920728f1650d1626'
				         }
				         */
				    }
				});

			});
		}
		else
			 res.redirect('/#/vuforia/'+req.body.markername+'/'+req.body.rowid+'/Upload only JPEG or PNG file formats');// redirecting the user to the same page
	});
	
	// A route for retrieving a specific record with a specific ID
	app.route('/data/:markerId').get(marker.read);
	// A route for updating a specific record with a specific ID
	app.route('/data/:markerId').put(marker.update);
	// A route for deleting a record in the MongoDB Collection with a specific ID
	app.route('/data/:markerId/:targetId').delete(function(req, res, next){
		client.deleteTarget(req.params.targetId, function (error, result) {
			//console.log(result);
		    /*
		     {
		        “result_code”:”Success”,
		        “transaction_id”:”550e8400e29b41d4a716446655482752”
		     }
		     */
		    if(result.result_code!="Success"){
				res.redirect('/#/data');
			}
			else
				marker.delete(req, res, next);
		});
	});
	// A route for getting a JSON response in the USER app for getting the details of the Mapped models along with all thier details
	app.route('/modeldownload/:id').get(marker.readforapp);
	// A route along with implemented function to upload the associated Model(s) with the Target marker image
	app.route('/uploadmodel').post(function(req,res){
		var modelArray = [];
		var x = 0;
		var modelUploaded = true;
		// Check if there are multiple models are being Uploaded
		if(req.files.model && (typeof req.files.model.length==='number' && (req.files.model.length%1)===0)) {
			for(x in req.files.model){
				if (!req.files.model[x]){
					modelUploaded = false;
					res.redirect('/#/modelupload/'+req.body.modelname+'/'+req.body.rowid);// redirecting the user to the same page
				}
				else if ((req.body.modeltype=='3D Model'&req.files.model[x].mimetype=='application/octet-stream')||(req.body.modeltype=='Video'&&req.files.model[x].mimetype=='video/mp4')||(req.body.modeltype=='Video'&&req.files.model[x].mimetype=='video/3gpp')||(req.body.modeltype=='Video'&&req.files.model[x].mimetype=='video/quicktime')){
					var modelx = req.files.model[x];
					var re = /(?:\.([^.]+))?$/;
					var ext = re.exec(req.files.model[x].name)[1];
					var filename = req.body.modelname+"("+x+")."+ext;

					var ACCESS_TOKEN = "H8Zb4c7ww5AAAAAAAAAACNen--4JW_Zom8X4MkALpuofwPIVjbGOn1l0yijxm6xC";
					var dbx = new Dropbox({ accessToken: ACCESS_TOKEN });
					dbx.filesUpload({path: '/' + filename, contents: req.files.model[x]})
						.then(function(response) {
							dbx.sharingCreateSharedLinkWithSettings({path: '/' + filename})
								.then(function(res){
									// console.log(res.url);
									modelArray[x] = res.url;

									// An obect which is being used for inserting the details (i.e. names of the uploaded model(s) and Target ID to which they are mapped) in the Database 
									var object={
											modelId: req.body.modelname,
											nameModel: modelArray,
											userName: req.user.userName,
										};
									//  A function call for inserting the details (which are in the above object) in the Database 			
									marker.modelUpdate(req.body.rowid, object);
									// Redirecting the User to the Dashboard

								})
								.catch(function(error){
									res.redirect('/#/modelupload/'+req.body.modelname+'/'+req.body.rowid);
									// console.log(error);
								});
							//console.log(response);
							if(modelUploaded){
			
								res.redirect('/#/home');
							}
						})
						.catch(function(error) {
							res.redirect('/#/modelupload/'+req.body.modelname+'/'+req.body.rowid);
							// console.error(error);
						});




					modelx.mv('./assets/models/'+filename, function(err) {
						if (err){
							modelUploaded = false;
						  	res.redirect('/#/modelupload/'+req.body.modelname+'/'+req.body.rowid);// redirecting the user to the same page
						}
					});


					if(ext == ".obj"){
						var matx = req.files.material[x];
						var materialFilename = req.body.modelname+"("+x+").mtl";
						matx.mv('./assets/models/'+materialFilename, function(err) {
							if (err){
								modelUploaded = false;
							  	res.redirect('/#/modelupload/'+req.body.modelname+'/'+req.body.rowid);// redirecting the user to the same page
							}
						});
					}
				}
				else{
					modelUploaded = false;
					res.redirect('/#/modelupload/'+req.body.modelname+'/'+req.body.rowid);// redirecting the user to the same page
				}
			}
		} 
		//check if there is one model is being uploaded and if there is none it will redirect user to the same page
		else{		
				if (!req.files.model){
					modelUploaded = false;
					res.redirect('/#/modelupload/'+req.body.modelname+'/'+req.body.rowid);// redirecting the user to the same page
				}
				else if ((req.body.modeltype=='3D Model'&&req.files.model.mimetype=='application/octet-stream')||(req.body.modeltype=='Video'&&req.files.model.mimetype=='video/mp4')||(req.body.modeltype=='Video'&&req.files.model.mimetype=='video/3gpp')||(req.body.modeltype=='Video'&&req.files.model.mimetype=='video/quicktime')){
					var modelx = req.files.model;
					var re = /(?:\.([^.]+))?$/;
					var ext = re.exec(req.files.model.name)[1];
					var filename = req.body.modelname+"("+x+")."+ext;



					var ACCESS_TOKEN = "H8Zb4c7ww5AAAAAAAAAACNen--4JW_Zom8X4MkALpuofwPIVjbGOn1l0yijxm6xC";
					var dbx = new Dropbox({ accessToken: ACCESS_TOKEN });
					// waterfall([
					// 	function(callback){
							dbx.filesUpload({path: '/' + filename, contents: req.files.model.data})
								.then(function(response) {
									dbx.sharingCreateSharedLinkWithSettings({path: '/' + filename})
										.then(function(res){
											// console.log(res.url);
											modelArray[0] = res.url;

											// An obect which is being used for inserting the details (i.e. names of the uploaded model(s) and Target ID to which they are mapped) in the Database 
											var object={
													modelId: req.body.modelname,
													nameModel: modelArray,
													userName: req.user.userName,
												};
											//  A function call for inserting the details (which are in the above object) in the Database 			
											marker.modelUpdate(req.body.rowid, object);
											// Redirecting the User to the Dashboard

										})
										.catch(function(error){
											res.redirect('/#/modelupload/'+req.body.modelname+'/'+req.body.rowid);
											// console.log(error);
										});
									//console.log(response);
									if(modelUploaded){
			
										res.redirect('/#/home');
									}
								})
								.catch(function(error) {
									res.redirect('/#/modelupload/'+req.body.modelname+'/'+req.body.rowid);
									// console.error(error);
								});
					// 		callback(null);
					// 	},
					// 	function(callback){
							
					// 		callback(err, 'done');
					// 	}
					// 	],function (err, result) {
					// 			if(err)
					// 				res.redirect('/#/modelupload/'+req.body.modelname+'/'+req.body.rowid);// redirecting the user to the same page
					// 		}
					// );



					modelx.mv('./assets/models/'+filename, function(err) {
						if (err){
							modelUploaded = false;
						  	res.redirect('/#/modelupload/'+req.body.modelname+'/'+req.body.rowid);// redirecting the user to the same page
						}
					});

					if(ext == "obj"){
						var matx = req.files.material;
						var materialFilename = req.body.modelname+"("+x+").mtl";
						matx.mv('./assets/models/'+materialFilename, function(err) {
							if (err){
								modelUploaded = false;
							  	res.redirect('/#/modelupload/'+req.body.modelname+'/'+req.body.rowid);// redirecting the user to the same page
							}
						});
					}
				}
				else{
					modelUploaded = false;
					res.redirect('/#/modelupload/'+req.body.modelname+'/'+req.body.rowid);// redirecting the user to the same page
				}
		}
		
		
		
	});

};