var vr = require('../../app/controllers/vr.server.controller');
var formidable = require('formidable');
var fs = require('fs');
var path = require('path');

module.exports = function(app){
		app.route('/vrdata').post(function(req, res){
			if (!req.files.projectphoto || !req.files.interiordesign || !req.files.script){
					res.redirect('/#/entervr');// redirecting the user to the same page
				}
			else{
					var projectphoto = req.files.projectphoto;
					projectphoto.mv('./assets/projectphoto/'+req.user.userName+req.files.projectphoto.name, function(err) {
						if (err){
						  	res.redirect('/#/entervr/');// redirecting the user to the same page
						}
					});
					var interiordesign = req.files.interiordesign;
					interiordesign.mv('./assets/interiordesign/'+req.user.userName+req.files.interiordesign.name, function(err) {
						if (err){
						  	res.redirect('/#/entervr/');// redirecting the user to the same page
						}
					});
					var script = req.files.script;
					script.mv('./assets/script/'+req.user.userName+req.files.script.name, function(err) {
						if (err){
						  	res.redirect('/#/entervr/');// redirecting the user to the same page
						}
					});
			}
			var vrRecord = {	
								projectName: req.body.projectname,
							    projectPhoto: req.files.projectphoto.name,
							    description: req.body.description,
							    interiorDesign: req.files.interiordesign.name,
							    script: req.files.script.name,
							    show: req.body.show,
							    isActive: true,
							    userName: req.user.userName
							};
			vr.create(vrRecord, res);
		});
	}