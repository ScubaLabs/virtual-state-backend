var user = require('../../app/controllers/user.server.controller');
var formidable = require('formidable');
var fs = require('fs');
var path = require('path');
var passport = require('passport');
var local = require('../../config/strategies/local');
var User = require('mongoose').model('UserDB');
var nodemailer = require('nodemailer');

module.exports = function(app){
	app.route('/signup').post(user.create);

	app.route('/change').post(user.passwordChange);

	app.route('/getuser').get(function(req,res){res.json({user:req.user})});
	
	app.route('/login').post(function(req, res, next) {
		passport.authenticate('local', function(err, user, info) {
		if (err) {
		  return next(err);
		}
		if (!user) {
		  return res.status(401).json({
		    err: info
		  });
		}
		req.logIn(user, function(err) {
		  if (err) {
		    return res.status(500).json({
		      err: 'Could not log in user'
		    });
		  }
		  res.status(200).json({
		    status: 'Login successful!',
		    user: user.userName
		  });
		});
		})(req, res, next);
	});
	app.route('/logout').get(function(req, res){
		req.logout();
		res.redirect('/#/loginscreen');
	});

	app.route('/status').get(function(req, res) {
		if (!req.isAuthenticated()) {
			return res.status(200).json({
			  status: false
			});
		}
		res.status(200).json({
		status: true
		});
	});

	app.route('/forgot').post(function(req, res, next) {
		async.waterfall([
		    function(done) {
			    crypto.randomBytes(20, function(err, buf) {
			        var token = buf.toString('hex');
			        done(err, token);
			    });
		    },
		    function(token, done) {
			    User.findOne({ userName: req.body.username }, function(err, user) {
			        if (!user) {
				        req.flash('error', 'No account with that email address exists.');
				        return res.redirect('/forgot.html');
			        }

			        user.resetPasswordToken = token;
			        user.resetPasswordExpires = Date.now() + 3600000; // 1 hour

			        user.save(function(err) {
			            done(err, token, user);
			        });
		        });
		    },
		    function(token, user, done) {
			    var smtpTransport = nodemailer.createTransport({
				        service: 'gmail',
				        auth: {
				          user: 'virtualestatetest@gmail.com',
				          pass: 'test@12345'
				        }
			        });
			    var mailOptions = {
				        to: user.email,
				        from: 'ayush@patchedpixels.com',
				        subject: 'Virtual Estate Password Reset',
				        text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
				          'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
				          'http://' + req.headers.host + '/reset/' + token + '\n\n' +
				          'If you did not request this, please ignore this email and your password will remain unchanged.\n'
				    };
			    smtpTransport.sendMail(mailOptions, function(err) {
			        req.flash('info', 'An e-mail has been sent to ' + user.email + ' with further instructions.');
			        done(err, 'done');
			    });
		    }
		  	], function(err) {
		    if (err) return next(err);
		    res.writeHead(200, {'Content-Type': 'text/html'});
    		res.end('A password reset link has been sent to your E-mail box.');
		});
	});

	app.route('/reset/:token').get(function(req, res) {
	  	User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
	    	if (!user) {
	      		req.flash('error', 'Password reset token is invalid or has expired.');
	      		return res.redirect('/forgot');
	    	}
	    	res.writeHead(200, {'Content-Type': 'text/html'});
		    res.write('<form method="post">');
		    res.write('<table style="margin:18% auto; border: solid; padding:2%;"><tr><td><h2 style="margin:auto">New Password:</h2></td><td><input type="password" name="password" style="width:100%;height:30px;display:block;margin: auto;"></td></tr>');
		    res.write('<tr><td><h2 style="margin:auto">Confirm Password:</h2></td><td><input type="password" name="cpassword" style="width:100%;height:30px;display:block;margin: auto;"></td></tr>');
		    res.write('<tr><td></td><td><input type="submit" style="font-size:22px; width:70%;height:40px;border:solid;"></td></tr></table>');
		    res.write('</form>');
	    	
		    return res.end();
	  	});
	});

	app.route('/reset/:token').post(function(req, res) {
  	    async.waterfall([
		    function(done) {
		      	User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
		        	if (!user) {
		          		req.flash('error', 'Password reset token is invalid or has expired.');
		          		return res.redirect('back');
		        	}

		        	if(!req.body.password){
		          		req.flash('error', 'Passwords do not match.');
		          		return res.redirect('back');
		        	}

		        	if (req.body.password !== req.body.cpassword) {
		          		req.flash('error', 'Passwords do not match.');
		          		return res.redirect('back');
		        	}

		        	user.password = req.body.password;
		        	user.resetPasswordToken = undefined;
		        	user.resetPasswordExpires = undefined;

		        	user.save(function(err) {
		          		done(err, user);
		        	});
		      	});
		    },
		    function(user, done) {
		      	var Transport = nodemailer.createTransport({
		        	service: 'gmail',
		        	auth: {
		          		user: 'virtualestatetest@gmail.com',
		          		pass: 'test@12345'
		        	}
		      	});
		      	var mailOptions = {
		        		to: user.email,
		        		from: 'virtualestatetest@gmail.com',
		        		subject: 'Your password has been changed',
		        		text: 'Hello,\n\n' +
		          		'This is a confirmation that the password for your account ' + user.email + ' has just been changed.\n'
		      		};
		      	Transport.sendMail(mailOptions, function(err) {
		        	req.flash('success', 'Success! Your password has been changed.');
		        	done(err);
		      	});
		    }
	  	],function(err) {
		    res.redirect('/');
		  });
	});
}